public class Arrays1 {
    public static void main(String... args) {
        // 1.4.23
        // Самая длинная возвышенность.

        /* Для заданного массива целых чисел определите длину и
         * местонахождение самой длинной непрерывной серии одинаковых значений,
         * до и после которой располагаются элементы с меньшим значением. */

        // Определяем массив для отладки.
        int[] a = { 5, 7, 8, 8, 8, 5, 8, 5 };

        // Определяем искомые переменные.
        int maxValue = a[0];
        int maxLength = 1;
        int position = 0;

        // Вычисляем искомые значения.
        int localLength = 1;
        for (int i = 1; i < a.length; ++i) {
            if (a[i] > maxValue) {
                maxValue = a[i];
                maxLength = 1;
                localLength = 1;
                position = i;
            } else if (a[i] == maxValue) {
                localLength++;
                if (localLength > maxLength) {
                    position = i - localLength + 1;
                    maxLength = localLength;
                }
            } else {
                localLength = 0;
            }
        }

        // Выводим искомые значения.
        System.out.println("maxValue: " + maxValue);
        System.out.println("maxLength: " + maxLength);
        System.out.println("position: " + position);
    }
}